package fr.geeklegend.hidozia.rendering;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.Display;

public class Renderer {
	
	public static void quadData(float x, float y, float w, float h, float[] color) {
		glColor4f(color[0], color[1], color[2], color[3]);
		glVertex2f(x, y);
		glVertex2f(x + w, y);
		glVertex2f(x + w, y + h);
		glVertex2f(x, y + h);
	}
	
	public static void renderQuad(float x, float y, float w, float h, float[] color) {
		glBegin(GL_QUADS);
			Renderer.quadData(x, y, w, h, color);
		glEnd();
	}
	
	public static void ortho() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 0, 1);
		glMatrixMode(GL_MODELVIEW);
	}
	
}
