package fr.geeklegend.hidozia.gui;

import static org.lwjgl.opengl.GL11.*;

import fr.geeklegend.hidozia.rendering.font.Font;

public class GUILabel {
	
	private String text;
	private float x, y;
	private int size;
	private float[] color;
	
	public GUILabel(float x, float y, int size, String text, float[] color) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.text = text;
		this.color = color;
	}
	
	public void render() {
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glColor3f(color[0], color[1], color[2]);
		Font.drawString(text, x, y, size);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public float[] getColor() {
		return color;
	}

	public void setColor(float[] color) {
		this.color = color;
	}
	
}
