package fr.geeklegend.hidozia.gui;

import fr.geeklegend.hidozia.math.Vec2;
import fr.geeklegend.hidozia.rendering.Renderer;

public class GUIButton {

	private Vec2 position;
	private Vec2 size;
	private float[] color;
	
	public GUIButton(float x, float y, float w, float h, float[] color) {
		this.position = new Vec2(x, y);
		this.size = new Vec2(w, h);
		this.color = color;
	}
	
	public void render() {
		Renderer.renderQuad(position.x, position.y, size.x, size.y, color);
	}

	public Vec2 getPosition() {
		return position;
	}

	public void setPosition(Vec2 position) {
		this.position = position;
	}

	public Vec2 getSize() {
		return size;
	}

	public void setSize(Vec2 size) {
		this.size = size;
	}

}
